DIR_X = { 'R':  0, 'L':  0, 'U': -1, 'D': +1 }
DIR_Y = { 'R': +1, 'L': -1, 'U':  0, 'D':  0 }

last = (0, 0)
ps: list[tuple[int, int]] = []

b = 0
while True:
    try:
        _, _, color = input().split()
        color = color.removeprefix('(#').removesuffix(')')
    except EOFError:
        break
    am = int(color[:5], 16)
    if color[-1] == '0': dir = 'R'
    elif color[-1] == '1': dir = 'D'
    elif color[-1] == '2': dir = 'L'
    elif color[-1] == '3': dir = 'U'
    cur = (last[0] + am*DIR_X[dir], last[1] + am*DIR_Y[dir])
    ps.append(cur)
    b += abs(last[0] - cur[0]) + abs(last[1] - cur[1])
    last = cur

def area(ps):
    n = len(ps)
    area = 0.0
    for i in range(n):
        j = (i+1) % n
        area += ps[i][0] * ps[j][1] - ps[j][0] * ps[i][1]
    area = abs(area) // 2
    return area

i = area(ps) - b/2 + 1
print(int(i+b))
