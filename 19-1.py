import operator
import re

VAR_TO_LAMBDA = {
    'x': operator.itemgetter(0),
    'm': operator.itemgetter(1),
    'a': operator.itemgetter(2),
    's': operator.itemgetter(3),
}

OP_TO_LAMBDA = {
    '<': operator.lt,
    '>': operator.gt,
}

def parse_rule(a):
    if ':' not in a: return (None, a)
    rule, target = a.split(':')
    var, op, val = re.split('(<|>)', rule)
    return ((var, op, int(val)), target)

ws = dict()
ps = []

workflow_part = True
while True:
    try:
        line = input()
        if line == "": workflow_part = False
        else:
            if workflow_part:
                j = line.find('{')
                name = line[:j]
                rules = [parse_rule(r) for r in line[j+1:-1].split(',')]
                ws[name] = rules
            else:
                parts = line.split(',')
                ps.append((int(parts[0][3:]), int(parts[1][2:]), int(parts[2][2:]), int(parts[3][2:-1])))
    except EOFError:
        break

ans = 0
for p in ps:
    cw = 'in'
    while cw not in ('R', 'A'):
        for rule, target in ws[cw]:
            if rule == None:
                cw = target
                break
            var, op, val = rule
            var_fn = VAR_TO_LAMBDA[var]
            op_fn = OP_TO_LAMBDA[op]
            if op_fn(var_fn(p), val):
                cw = target
                break
    if cw == 'A':
        ans += p[0] + p[1] + p[2] + p[3]

print(ans)
