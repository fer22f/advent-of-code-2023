from math import lcm

instructions = input()
input()

go_r = dict()
go_l = dict()
nodes = set()

while True:
    try:
        node, leftright = input().split(' = ')
    except EOFError:
        break
    nodes.add(node)
    left, right = leftright.removeprefix('(').removesuffix(')').split(', ')
    go_l[node] = left
    go_r[node] = right

cur = []
for node in nodes:
    if node.endswith('A'):
        cur.append(node)

cycles = []
for node in cur:
    c = 0
    while True:
        for inst in instructions:
            c += 1
            if inst == 'L':
                node = go_l[node]
            else:
                node = go_r[node]
        if node.endswith('Z'):
            cycles.append(c)
            print(c)
            c = 0
            break

print(lcm(*cycles))
