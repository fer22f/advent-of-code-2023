grid = []

while True:
    try:
        row = input()
    except EOFError:
        break
    grid.append([cell for cell in row])

n = len(grid)
m = len(grid[0])

def sum_here(i, j):
    if not (0 <= i < n): return 0
    if not (0 <= j < m): return 0
    if not ('0' <= grid_copy[i][j] <= '9'): return 0
    for jj in reversed(range(0, j)):
        if not ('0' <= grid_copy[i][jj] <= '9'):
            break
    if not ('0' <= grid_copy[i][jj] <= '9'):
        jj += 1

    d = ''
    for k in range(jj, m):
        if '0' <= grid_copy[i][k] <= '9':
            d += grid_copy[i][k]
            grid_copy[i][k] = 'x'
        else:
            break
    return int(d)

ans = 0
for i in range(n):
    for j in range(m):
        if grid[i][j] == '*':
            grid_copy = [[cell for cell in row] for row in grid]
            cans = [x for x in [
                sum_here(i+1, j),
                sum_here(i-1, j),
                sum_here(i, j+1),
                sum_here(i, j-1),
                sum_here(i+1, j+1),
                sum_here(i-1, j+1),
                sum_here(i+1, j-1),
                sum_here(i-1, j-1),
            ] if x != 0]
            if len(cans) == 2:
                ans += cans[0]*cans[1]

print(ans)
