def read_map(title):
    assert(input() == f'{title} map:')
    a = []
    while True:
        try:
            line = input()
        except EOFError:
            break
        if line == "": break
        dest, src, sz = map(int, line.split())
        a.append((src, dest, sz))
    return sorted(a)

def compact_ranges(ranges):
    ranges = sorted(ranges)
    new_ranges = [ranges[0]]
    for start, end in ranges[1:]:
        if new_ranges[-1][1] >= start:
            new_ranges[-1] = (new_ranges[-1][0], end)
        else:
            new_ranges.append((start, end))
    return new_ranges

seeds = map(int, input().removeprefix("seeds: ").split())
seeds = list(zip(seeds, seeds))
input()
seed_to_soil = read_map('seed-to-soil')
soil_to_fertilizer = read_map('soil-to-fertilizer')
fertilizer_to_water = read_map('fertilizer-to-water')
water_to_light = read_map('water-to-light')
light_to_temperature = read_map('light-to-temperature')
temperature_to_humidity = read_map('temperature-to-humidity')
humidity_to_location = read_map('humidity-to-location')

ranges = compact_ranges([(start, start+sz) for start, sz in seeds])
for to in (
    seed_to_soil, soil_to_fertilizer, fertilizer_to_water, water_to_light,
    light_to_temperature, temperature_to_humidity, humidity_to_location
):
    new_ranges = []
    for start, end in ranges:
        cur = start
        for src, dest, sz in to:
            if cur < min(src, end):
                new_ranges.append((cur, min(src, end)))
            new_start = dest+max(cur, src)-src
            new_end = dest+min(src+sz, end)-src
            if new_start < new_end:
                new_ranges.append((new_start, new_end))
            if cur < src+sz:
                cur = src+sz
        if cur < end:
            new_ranges.append((cur, end))
    ranges = compact_ranges(new_ranges)

print(ranges[0][0])
