from math import lcm
from collections import defaultdict, deque

mod = defaultdict(str)
mod_outputs = defaultdict(list)
mod_inputs = defaultdict(list)

while True:
    try:
        modtype_name, outputs_unparsed = input().split(' -> ')
    except EOFError:
        break
    outputs = outputs_unparsed.split(', ')
    if modtype_name == 'broadcaster':
        mod['broadcaster'] = 'broadcaster'
        mod_outputs[modtype_name] = outputs
        for output in outputs:
            mod_inputs[output].append(modtype_name)
    else:
        modtype = modtype_name[0]
        name = modtype_name[1:]
        mod[name] = modtype
        mod_outputs[name] = outputs
        for output in outputs:
            mod_inputs[output].append(modtype_name[1:])

mod_memory = defaultdict(dict)
for name, outputs in mod_outputs.items():
    for output in outputs:
        if mod[output] == '&':
            mod_memory[output][name] = 'low'

mod_state = defaultdict(bool)

print(f'{mod["rx"]}rx: {mod_inputs["rx"]}')
before = mod_inputs['rx'][0]
before_inputs = mod_inputs[before]
print(f'{mod[before]}{before}: {mod_inputs[before]}')

last_high = defaultdict(int)
mod_high = defaultdict(int)

for i in range(10000):
    q = deque()
    q.append(('broadcaster', 'low', 'button'))
    while len(q):
        name, pulse, origin = q.popleft()
        if origin in before_inputs and pulse != 'low':
            print(origin, i - last_high[origin])
            mod_high[origin] = i - last_high[origin]
            last_high[origin] = i
        if mod[name] == 'broadcaster':
            new_pulse = pulse
        elif mod[name] == '&':
            mod_memory[name][origin] = pulse
            new_pulse = 'low' if all(pulse == 'high' for pulse in mod_memory[name].values()) else 'high'
        elif mod[name] == '%':
            if pulse == 'high': continue
            mod_state[name] = not mod_state[name]
            if mod_state[name]:
                new_pulse = 'high'
            else:
                new_pulse = 'low'
        for output in mod_outputs[name]:
            q.append((output, new_pulse, name))

print(lcm(*mod_high.values()))
