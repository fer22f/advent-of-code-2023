a = []
while True:
    try:
        a.append(list(input()))
    except EOFError:
        break
n = len(a)
m = len(a[0])

for j in range(m):
    last = -1
    for i in range(0, n):
        if last != -1 and a[i][j] == 'O':
            a[last][j] = 'O'
            a[i][j] = '.'
            if last+1 < n and a[last+1][j] == '.':
                last += 1
            else:
                last = -1
        if last == -1 and a[i][j] == '.':
            last = i
        if a[i][j] == '#':
            last = -1


ans = 0
for i in range(n):
    for j in range(m):
        if a[i][j] == 'O':
            ans += n-i

print(ans)
