import regex

def digit(s):
    if '0' <= s <= '9': return int(s)
    if s == 'one': return 1
    if s == 'two': return 2
    if s == 'three': return 3
    if s == 'four': return 4
    if s == 'five': return 5
    if s == 'six': return 6
    if s == 'seven': return 7
    if s == 'eight': return 8
    if s == 'nine': return 9

ans = 0
while True:
    try:
        s = input()
    except EOFError:
        break

    digits = [digit(x) for x in regex.findall('[0-9]|one|two|three|four|five|six|seven|eight|nine', s, overlapped=True)]
    x = 10*digits[0] + digits[-1]
    ans += x

print(ans)
