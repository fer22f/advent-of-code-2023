sequence = input().split(',')
ans = 0
for step in sequence:
    h = 0
    for c in step:
        h += ord(c)
        h *= 17
        h %= 256
    ans += h
print(ans)
