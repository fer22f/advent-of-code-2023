def solve(i, j):
    global l
    if i == len(l):
        return 1 if j == len(s) else 0
    if j == len(s):
        return solve(i+1, j) if (l[i] == '.' or l[i] == '?') else 0
    ans = 0
    if (i+s[j] < len(l) and all(c == '?' or c == '#' for c in l[i:i+s[j]])
        and (l[i+s[j]] == '?' or l[i+s[j]] == '.')):
        ans += solve(i+s[j]+1, j+1)
    if l[i] == '.' or l[i] == '?':
        ans += solve(i+1, j)
    return ans

ans = 0
while True:
    try:
        l, s = input().split()
        l = [c for c in l]
    except EOFError:
        break
    l += '.'
    s = [int(segment) for segment in s.split(',')]
    ans += solve(0, 0)

print(ans)
