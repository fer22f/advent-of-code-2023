from collections import defaultdict
import heapq as hq

a = []
while True:
    try: a.append([int(c) for c in input()])
    except EOFError: break

oo = 987654231987654321
n = len(a)
m = len(a[0])

DIR_X = { '>':  0, '<':  0, '^': -1, 'v': +1 }
DIR_Y = { '>': +1, '<': -1, '^':  0, 'v':  0 }

visited = set()
d = defaultdict(lambda: oo)
queue = []
hq.heappush(queue, (0, (0, 0, '>', 0)))
d[(0, 0, '>', 0)] = 0

while len(queue) > 0:
    g, u = hq.heappop(queue)
    visited.add(u)
    x, y, dir, am = u
    if dir == '>':
        go = ['^', '>', 'v']
    elif dir == 'v':
        go = ['<', 'v', '>']
    elif dir == '<':
        go = ['^', '<', 'v']
    else:
        assert(dir == '^')
        go = ['<', '^', '>']

    for ndir in go:
        nx = x + DIR_X[ndir]
        ny = y + DIR_Y[ndir]
        nam = am+1 if ndir == dir else 1
        v = (nx, ny, ndir, nam)
        if 0 <= nx < n and 0 <= ny < m and nam <= 3 and v not in visited and g+a[nx][ny] < d[v]:
            d[v] = g+a[nx][ny]
            hq.heappush(queue, (d[v], v))

ans = oo
for (x, y, dir, am), dist in d.items():
    if x == n-1 and y == m-1:
        ans = min(ans, dist)
print(ans)
