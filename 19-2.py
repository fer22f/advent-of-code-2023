from pprint import pprint
from collections import defaultdict
import re

def parse_rule(a):
    if ':' not in a: return (None, a)
    rule, target = a.split(':')
    var, op, val = re.split('(<|>)', rule)
    return ((var, op, int(val)), target)

ws = dict()
ps = []

while True:
    line = input()
    if line == "": break
    j = line.find('{')
    name = line[:j]
    rules = [parse_rule(r) for r in line[j+1:-1].split(',')]
    ws[name] = rules

a_restrictions = []
def dfs(u, acc):
    if u in ('R', 'A'):
        if u == 'A':
            a_restrictions.append(acc)
        return
    for rule, target in ws[u]:
        if rule == None:
            dfs(target, acc)
            break
        var, op, val = rule
        dfs(target, [*acc, rule])
        acc.append((var, '<' if op == '>' else '>', val+1 if op == '>' else val-1))

dfs('in', [])
ans = 0
for restrictions in a_restrictions:
    print(restrictions)
    compacted_min = dict()
    compacted_max = dict()
    for var, op, val in restrictions:
        if op == '>':
            if var in compacted_min:
                compacted_min[var] = max(compacted_min[var], val)
            else:
                compacted_min[var] = val
        else:
            if var in compacted_max:
                compacted_max[var] = min(compacted_max[var], val)
            else:
                compacted_max[var] = val
    cans = 1
    for var in ('x', 'm', 'a', 's'):
        mn = compacted_min.get(var, 0)
        mx = compacted_max.get(var, 4001)
        if mn > mx:
            print('broken')
            break
        cans *= mx-mn-1
        print(f'    {mn} < {var} < {mx}')
        print(f'      {mx-mn-1}')
    print(f'  {cans}')
    ans += cans
print(ans)
