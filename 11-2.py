a = []

while True:
    try:
        a.append([c for c in input()])
    except EOFError:
        break

n = len(a)
m = len(a[0])

stars = []
dist_row = []
for i in range(n):
    stars_in_row = 0
    for j in range(m):
        if a[i][j] == '#':
            stars_in_row += 1
            stars.append((i, j))
    dist_row.append(1000000 if stars_in_row == 0 else 1)

dist_col = []
for j in range(m):
    stars_in_col = 0
    for i in range(n):
        if a[i][j] == '#':
            stars_in_col += 1
    dist_col.append(1000000 if stars_in_col == 0 else 1)

ans = 0
for ii, (x1, y1) in enumerate(stars):
    for jj, (x2, y2) in enumerate(stars[ii+1:]):
        lx = min(x1, x2)
        rx = max(x1, x2)
        dx = 0
        for i in range(lx, rx):
            dx += dist_row[i]
        ly = min(y1, y2)
        ry = max(y1, y2)
        dy = 0
        for i in range(ly, ry):
            dy += dist_col[i]
        ans += dx + dy

print(ans)
