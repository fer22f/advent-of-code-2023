from functools import cache

@cache
def solve(ix, i, j):
    if i == len(l):
        return 1 if j == len(s) else 0
    if j == len(s):
        return solve(ix, i+1, j) if (l[i] == '.' or l[i] == '?') else 0
    ans = 0
    if (i+s[j] < len(l) and all(c == '?' or c == '#' for c in l[i:i+s[j]])
        and (l[i+s[j]] == '?' or l[i+s[j]] == '.')):
        ans += solve(ix, i+s[j]+1, j+1)
    if l[i] == '.' or l[i] == '?':
        ans += solve(ix, i+1, j)
    return ans

ix = 1
ans = 0
while True:
    try:
        l, s = input().split()
        l = [c for c in l]
    except EOFError:
        break
    s = [int(segment) for segment in s.split(',')]
    l += '?'
    l = l*5
    l[-1] = '.'
    l = ''.join(l)
    s = s*5
    ans += solve(ix, 0, 0)
    ix += 1

print(ans)
