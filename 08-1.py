instructions = input()
input()

go_r = dict()
go_l = dict()

while True:
    try:
        node, leftright = input().split(' = ')
    except EOFError:
        break
    left, right = leftright.removeprefix('(').removesuffix(')').split(', ')
    go_l[node] = left
    go_r[node] = right

node = 'AAA'
c = 0
while node != 'ZZZ':
    for inst in instructions:
        c += 1
        if inst == 'L':
            node = go_l[node]
        else:
            node = go_r[node]
        if node == 'ZZZ': break

print(c)
