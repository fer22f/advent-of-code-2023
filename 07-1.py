from pprint import pprint
from collections import Counter

power = {
    'A': 13, 'K': 12, 'Q': 11, 'J': 10, 'T': 9, '9': 8, '8': 7, '7': 6,
    '6': 5, '5': 4, '4': 3, '3': 2, '2': 1,
}

def get_hand_type(hand):
    card_count = sorted(Counter(hand).values())
    match card_count:
        case [5]:
            # five of a kind
            return 7
        case [1, 4]:
            # four of a kind
            return 6
        case [2, 3]:
            # full house
            return 5
        case [1, 1, 3]:
            # three of a kind
            return 4
        case [1, 2, 2]:
            # two pair
            return 3
        case [1, 1, 1, 2]:
            # one pair
            return 2
        case [1, 1, 1, 1, 1]:
            # high card
            return 1

hands = []
while True:
    try:
        hand, bid = input().split()
    except EOFError:
        break
    bid = int(bid)
    cards = [power[card] for card in hand]
    hands.append((get_hand_type(hand), cards, bid))

ans = 0
for rank, (_, _, bid) in enumerate(sorted(hands), 1):
    ans += rank * bid
print(ans)
