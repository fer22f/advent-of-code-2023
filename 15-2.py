sequence = input().split(',')
ans = 0
boxes = [list() for _ in range(256)]
for step in sequence:
    if '=' in step:
        label, _ = step.split('=')
    else:
        assert(step.endswith('-'))
        label = step.removesuffix('-')
    box = 0
    for c in label:
        box += ord(c)
        box *= 17
        box %= 256
    if '=' in step:
        _, focal = step.split('=')
        focal = int(focal)
        for i, (clabel, cfocal) in enumerate(boxes[box]):
            if clabel == label:
                boxes[box] = boxes[box][:i] + [(label, focal)] + boxes[box][i+1:]
                break
        else:
            boxes[box].append((label, focal))
    else:
        for i, (clabel, cfocal) in enumerate(boxes[box]):
            if clabel == label:
                boxes[box] = boxes[box][:i] + boxes[box][i+1:]

ans = 0
for boxi, box in enumerate(boxes, 1):
    for i, (label, focal) in enumerate(box, 1):
        ans += boxi * i * focal
print(ans)
