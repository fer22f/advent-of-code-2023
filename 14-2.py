from collections import defaultdict

a = []
while True:
    try:
        a.append(list(input()))
    except EOFError:
        break
n = len(a)

def move_north():
    for j in range(n):
        last = -1
        for i in range(0, n):
            if last != -1 and a[i][j] == 'O':
                a[last][j] = 'O'
                a[i][j] = '.'
                if last+1 < n and a[last+1][j] == '.':
                    last += 1
                else:
                    last = -1
            if last == -1 and a[i][j] == '.':
                last = i
            if a[i][j] == '#':
                last = -1

def rotate_90():
    global a
    b = [row[:] for row in a]
    for i in range(n):
        for j in range(n):
            b[i][j] = a[n-1-j][i]
    a = b

visited = dict()
visited_times = defaultdict(int)
grid_at = dict()
for it in range(0, 1000000000):
    for k in range(4):
        move_north()
        rotate_90()
    grid_at[it] = [row[:] for row in a]
    grid = '\n'.join(''.join(row) for row in a)
    if grid in visited:
        print(f'iteration {it}: already visited this grid at iteration {visited[grid]}')
        visited_times[grid] += 1
        if visited_times[grid] == 1:
            cycle_size = it - visited[grid]
            start_cycle = visited[grid]
            break
    else:
        visited[grid] = it

print(f'start={start_cycle} size={cycle_size}')
want = ((1000000000 - start_cycle) % cycle_size)
a = grid_at[start_cycle + want - 1]

ans = 0
for i in range(n):
    for j in range(n):
        if a[i][j] == 'O':
            ans += n-i

print(ans)
