ans = 0

while True:
    try:
        line = input()
    except EOFError:
        break

    card, numbers = line.split(':')
    winning, have = [[int(x) for x in xs.split()] for xs in numbers.split('|')]
    wins = 0
    for number in have:
        if number in winning:
            wins += 1
    if wins > 0:
        ans += 2**(wins-1)

print(ans)
