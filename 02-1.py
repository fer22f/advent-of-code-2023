ans = 0

while True:
    try:
        s = input()
    except EOFError:
        break

    game, sets_cubes = s.split(':')
    i = int(game.removeprefix('Game '))
    ok = True
    for set_cubes in sets_cubes.split(';'):
        bag = {
            'red': 12,
            'green': 13,
            'blue': 14,
        }
        for cube in set_cubes.split(','):
            quantity, color = cube.split()
            quantity = int(quantity)
            bag[color] -= quantity
            if bag[color] < 0:
                ok = False

    if ok: ans += i

print(ans)
