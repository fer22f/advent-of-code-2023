from collections import deque

oo = 987654321987654321

a = []
while True:
    try:
        row = [c for c in input()]
    except EOFError:
        break
    a.append(row)

n = len(a)
m = len(a[0])

for i in range(n):
    for j in range(m):
        if a[i][j] == 'S':
            si = i
            sj = j

dist = [[oo]*m for _ in range(n)]

q = deque()
q.appendleft((si, sj))
dist[si][sj] = 0
a[si][sj] = '7'

def insert(i, j, d):
    if not (0 <= i < n): return
    if not (0 <= j < m): return
    if dist[i][j] != oo: return
    dist[i][j] = d + 1
    q.appendleft((i, j))

while len(q):
    i, j = q[-1]
    q.pop()

    match a[i][j]:
        case '|':
            insert(i-1, j, dist[i][j])
            insert(i+1, j, dist[i][j])
        case '-':
            insert(i, j-1, dist[i][j])
            insert(i, j+1, dist[i][j])
        case 'L':
            insert(i-1, j, dist[i][j])
            insert(i, j+1, dist[i][j])
        case 'J':
            insert(i-1, j, dist[i][j])
            insert(i, j-1, dist[i][j])
        case '7':
            insert(i+1, j, dist[i][j])
            insert(i, j-1, dist[i][j])
        case 'F':
            insert(i+1, j, dist[i][j])
            insert(i, j+1, dist[i][j])
        case '.':
            print('what?')

b = []
for i in range(n):
    b.append([a[i][j] if dist[i][j] != oo else '.' for j in range(m)])

print('\n'.join(''.join(b[i]) for i in range(n)))

ans = 0
for i in range(n):
    q = 0
    for j in range(m):
        if b[i][j] == '.':
            ans += q % 2
        if b[i][j] in ('|', 'L', 'J'):
            q += 1

print(ans)
