t = int(input().removeprefix("Time:").replace(' ', ''))
d = int(input().removeprefix("Distance:").replace(' ', ''))

def solve(t, d):
    lo = 0
    hi = int(1e15)
    while lo < hi:
        mi = (lo + hi)//2
        cur = (t-mi)*mi
        nxt = (t-(mi+1))*(mi+1)
        if cur <= nxt:
            lo = mi + 1
        else:
            hi = mi

    midpoint = lo

    lo = 0
    hi = midpoint
    while lo < hi:
        mi = (lo + hi)//2
        if (t-mi)*mi <= d:
            lo = mi + 1
        else:
            hi = mi

    start = lo

    lo = midpoint
    hi = t+1
    while lo < hi:
        mi = (lo + hi)//2
        if (t-mi)*mi > d:
            lo = mi + 1
        else:
            hi = mi

    end = lo-1

    return end-start+1

print(solve(t, d))
