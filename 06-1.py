time = map(int, input().removeprefix("Time:").split())
dist = map(int, input().removeprefix("Distance:").split())

def solve(t, d):
    lo = 0
    hi = int(1e15)
    while lo < hi:
        mi = (lo + hi)//2
        cur = (t-mi)*mi
        nxt = (t-(mi+1))*(mi+1)
        if cur <= nxt:
            lo = mi + 1
        else:
            hi = mi

    midpoint = lo

    lo = 0
    hi = midpoint
    while lo < hi:
        mi = (lo + hi)//2
        if (t-mi)*mi <= d:
            lo = mi + 1
        else:
            hi = mi

    start = lo

    lo = midpoint
    hi = t+1
    while lo < hi:
        mi = (lo + hi)//2
        if (t-mi)*mi > d:
            lo = mi + 1
        else:
            hi = mi

    end = lo-1

    return end-start+1

ans = 1
sans = 1
for (t, d) in zip(time, dist):
    ways = 0
    for h in range(1, t+1):
        travel = (t - h)*h
        if travel > d:
            ways += 1
    ans *= ways
    sans *= solve(t, d)

print(ans)
print(sans)
