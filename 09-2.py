from collections import deque

ans = 0

while True:
    try:
        seq = [int(x) for x in input().split()]
    except EOFError:
        break

    seqs = [deque(seq)]
    for i in range(10000000):
        newseq = []
        seq = seqs[i]
        nxt = iter(seq)
        next(nxt)
        for last, cur in zip(nxt, seq):
            newseq.append(last-cur)
        seqs.append(deque(newseq))
        if all(x == 0 for x in newseq):
            break
    seqs[-1].appendleft(0)
    for i in reversed(range(len(seqs)-1)):
        seqs[i].appendleft(seqs[i][0] - seqs[i+1][0])
    ans += seqs[0][0]

print(ans)
