def read_map(title):
    assert(input() == f'{title} map:')
    a = []
    while True:
        try:
            line = input()
        except EOFError:
            break
        if line == "": break
        dest, src, sz = map(int, line.split())
        a.append((src, dest, sz))
    return sorted(a)

oo = 986754321

def lookup(to, i):
    for src, dest, sz in to:
        if src <= i < src+sz:
            return dest+i-src
    else:
        return i

seeds = map(int, input().removeprefix("seeds: ").split())
input()
seed_to_soil = read_map('seed-to-soil')
soil_to_fertilizer = read_map('soil-to-fertilizer')
fertilizer_to_water = read_map('fertilizer-to-water')
water_to_light = read_map('water-to-light')
light_to_temperature = read_map('light-to-temperature')
temperature_to_humidity = read_map('temperature-to-humidity')
humidity_to_location = read_map('humidity-to-location')

ans = oo

for seed in seeds:
    soil = lookup(seed_to_soil, seed)
    fertilizer = lookup(soil_to_fertilizer, soil)
    water = lookup(fertilizer_to_water, fertilizer)
    light = lookup(water_to_light, water)
    temperature = lookup(light_to_temperature, light)
    humidity = lookup(temperature_to_humidity, temperature)
    location = lookup(humidity_to_location, humidity)
    ans = min(ans, location)

print(ans)
