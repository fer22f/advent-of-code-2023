from collections import defaultdict, deque

mod = defaultdict(str)
mod_outputs = defaultdict(list)

while True:
    try:
        modtype_name, outputs_unparsed = input().split(' -> ')
    except EOFError:
        break
    outputs = outputs_unparsed.split(', ')
    if modtype_name == 'broadcaster':
        mod['broadcaster'] = 'broadcaster'
        mod_outputs[modtype_name] = outputs
    else:
        modtype = modtype_name[0]
        name = modtype_name[1:]
        mod[name] = modtype
        mod_outputs[name] = outputs

mod_memory = defaultdict(dict)
for name, outputs in mod_outputs.items():
    for output in outputs:
        if mod[output] == '&':
            mod_memory[output][name] = 'low'

mod_state = defaultdict(bool)

count = defaultdict(int)
for i in range(1000):
    q = deque()
    q.append(('broadcaster', 'low', 'button'))
    while len(q):
        name, pulse, origin = q.popleft()
        count[pulse] += 1
        match mod[name]:
            case 'broadcaster':
                new_pulse = pulse
            case '&':
                mod_memory[name][origin] = pulse
                new_pulse = 'low' if all(pulse == 'high' for pulse in mod_memory[name].values()) else 'high'
            case '%':
                if pulse == 'high': continue
                mod_state[name] = not mod_state[name]
                if mod_state[name]:
                    new_pulse = 'high'
                else:
                    new_pulse = 'low'
        for output in mod_outputs[name]:
            q.append((output, new_pulse, name))

ans = count['low'] * count['high']
print(ans)
