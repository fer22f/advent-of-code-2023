from collections import defaultdict

ans = 0

mult = defaultdict(lambda: 1)

while True:
    try:
        line = input()
    except EOFError:
        break

    card, numbers = line.split(':')
    i = int(card.removeprefix('Card '))
    winning, have = [[int(x) for x in xs.split()] for xs in numbers.split('|')]
    wins = 0
    for number in have:
        if number in winning:
            wins += 1
            mult[i+wins] += mult[i]
    ans += mult[i]

print(ans)
