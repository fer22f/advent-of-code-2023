ans = 0

while True:
    try:
        seq = [int(x) for x in input().split()]
    except EOFError:
        break

    seqs = [seq]
    for i in range(10000000):
        newseq = []
        seq = seqs[i]
        nxt = iter(seq)
        next(nxt)
        for last, cur in zip(nxt, seq):
            newseq.append(last-cur)
        seqs.append(newseq)
        if all(x == 0 for x in newseq):
            break
    seqs[-1].append(0)
    for i in reversed(range(len(seqs)-1)):
        seqs[i].append(seqs[i][-1] + seqs[i+1][-1])
    ans += seqs[0][-1]

print(ans)
