ans = 0
while True:
    try:
        s = input()
    except EOFError:
        break

    digits = [int(x) for x in s if '0' <= x <= '9']
    x = 10*digits[0] + digits[-1]
    ans += x

print(ans)
