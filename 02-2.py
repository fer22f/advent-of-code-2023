ans = 0

while True:
    try:
        s = input()
    except EOFError:
        break

    game, sets_cubes = s.split(':')
    i = int(game.removeprefix('Game '))
    bag = { 'red': 0, 'green': 0, 'blue': 0 }
    for set_cubes in sets_cubes.split(';'):
        for cube in set_cubes.split(','):
            quantity, color = cube.split()
            quantity = int(quantity)
            bag[color] = max(bag[color], quantity)

    ans += bag['red'] * bag['green'] * bag['blue']

print(ans)
