a = []
while True:
    try:
        a.append([c for c in input()])
    except EOFError:
        break

n = len(a)
m = len(a[0])

q: list[tuple[int, int, str]] = [(0, 0, '>')]
vis = set()
vis.add(q[-1])

DIR_X = { '>':  0, '<':  0, '^': -1, 'v': +1 }
DIR_Y = { '>': +1, '<': -1, '^':  0, 'v':  0 }

b = [r[:] for r in a]
c = [['.' for _ in range(n)] for _ in range(m)]

FORWARD_SLASH_DIR = { '>': '^', '<': 'v', 'v': '<', '^': '>' }
BACKWARD_SLASH_DIR = { '>': 'v', '<': '^', 'v': '>', '^': '<' }

def add(nx, ny, d):
    node = (nx, ny, d)
    if 0 <= nx < n and 0 <= ny < m and node not in vis:
        vis.add(node)
        q.append(node)

while len(q):
    x, y, d = q[-1]
    c[x][y] = '#'
    q.pop()
    if a[x][y] == '.':
        add(x+DIR_X[d], y+DIR_Y[d], d)
        if b[x][y] == '.':
            b[x][y] = d
        else:
            b[x][y] = '2'
    elif a[x][y] == '/':
        nd = FORWARD_SLASH_DIR[d]
        add(x+DIR_X[nd], y+DIR_Y[nd], nd)
    elif a[x][y] == '\\':
        nd = BACKWARD_SLASH_DIR[d]
        add(x+DIR_X[nd], y+DIR_Y[nd], nd)
    elif a[x][y] == '|':
        if d == '>' or d == '<':
            add(x-1, y, '^')
            add(x+1, y, 'v')
        else:
            add(x+DIR_X[d], y+DIR_Y[d], d)
    else:
        assert(a[x][y] == '-')
        if d == '^' or d == 'v':
            add(x, y-1, '<')
            add(x, y+1, '>')
        else:
            add(x+DIR_X[d], y+DIR_Y[d], d)

print('\n'.join(''.join(r) for r in b))
print('\n'.join(''.join(r) for r in c))

print(sum(1 for row in c for ch in row if ch == '#'))
