from collections import defaultdict

done = False

def find_reflections(rows):
    refl = defaultdict(int)
    for row in rows:
        for j in range(1, len(row)):
            sz = min(j, len(row)-j)
            if row[j-sz:j][::-1] == row[j:j+sz]:
                refl[j] += 1
    for i, c in refl.items():
        if c == len(rows):
            return i
    return None

ans = 0

while not done:
    rows = []
    while True:
        try:
            row = input()
        except EOFError:
            done = True
            break
        if row == '': break
        rows.append(row)

    hor = find_reflections(rows)
    if hor is not None:
        ans += hor
    cols = list(''.join(col) for col in zip(*rows))
    ver = find_reflections(cols)
    if ver is not None:
        ans += 100*ver

print(ans)
