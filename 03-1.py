grid = []

while True:
    try:
        row = input()
    except EOFError:
        break
    grid.append([cell for cell in row])

n = len(grid)
m = len(grid[0])

def sum_here(i, j):
    if not (0 <= i < n): return 0
    if not (0 <= j < m): return 0
    if not ('0' <= grid[i][j] <= '9'): return 0
    for jj in reversed(range(0, j)):
        if not ('0' <= grid[i][jj] <= '9'):
            break
    if not ('0' <= grid[i][jj] <= '9'):
        jj += 1

    d = ''
    for k in range(jj, m):
        if '0' <= grid[i][k] <= '9':
            d += grid[i][k]
            grid[i][k] = 'x'
        else:
            break
    return int(d)

ans = 0
for i in range(n):
    for j in range(m):
        if grid[i][j] != '.' and not ('0' <= grid[i][j] <= '9'):
            ans += sum_here(i+1, j)
            ans += sum_here(i-1, j)
            ans += sum_here(i, j+1)
            ans += sum_here(i, j-1)
            ans += sum_here(i+1, j+1)
            ans += sum_here(i-1, j+1)
            ans += sum_here(i+1, j-1)
            ans += sum_here(i-1, j-1)

print(ans)
